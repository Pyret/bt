<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-24
 * Time: 21:18
 */
return[

    'mode' => 'Development Mode',

    'app' => [
	'url' => 'http://78.71.226.194',
    ] ,

    'site' =>[
	'name' => 'En lite sida',
	'under_construction' => false,
	'beta' => true,
	'toplinks' => true,
	'avatar' => false
    ],

    'database' => [
	'driver' => 'mysql',
	'host' => '78.71.226.194',
	'name' => 'bt',
	'username' => 'user',
	'password' => 'password',
	'charset' => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix' => ''
    ],

    'log' => [
	'debug' => true
    ],
    'twig' => [
	'debug' => true
    ],
];